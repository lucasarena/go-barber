import { startOfDay, endOfDay, parseISO } from 'date-fns';
import { Op } from 'sequelize';

import Appointment from '../models/Appointment';
import User from '../models/User';

class ScheduleController {
    async index(req, res) {
        const isProvider = await User.findAll({
            where: { id: req.userId, provider: true },
        });

        if (!isProvider) {
            res.status(401).json({ error: 'User is not a provider' });
        }

        const { date } = req.query;
        const parseDate = parseISO(date);

        const schedule = await Appointment.findAll({
            where: {
                provider_id: req.userId,
                canceled_at: null,
                date: {
                    [Op.between]: [startOfDay(parseDate), endOfDay(parseDate)],
                },
            },
            include: {
                model: User,
                as: 'user',
                attributes: ['name'],
            },
            order: ['date'],
        });

        return res.json(schedule);
    }
}

export default new ScheduleController();
