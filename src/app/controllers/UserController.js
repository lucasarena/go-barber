import * as Yup from 'yup';

import User from '../models/User';
import File from '../models/File';

class UserController {
    async store(req, res) {
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string()
                .email()
                .required(),
            password: Yup.string().required(),
            provider: Yup.bool(),
        });

        if (!(await schema.isValid(req.body))) {
            res.status(400).json({ error: 'Invalid request' });
        }

        const { name, email, password, provider } = req.body;

        const userExists = await User.findOne({ where: { email } });

        if (userExists) {
            res.status(400).json({ error: 'User exists' });
        }

        await User.create({
            name,
            email,
            password,
            provider,
        });

        return res.json({
            name,
            email,
            provider,
        });
    }

    async update(req, res) {
        const {
            name,
            email,
            provider,
            password,
            oldPassword,
            avatar_id,
        } = req.body;

        const user = await User.findByPk(req.userId);

        if (!user) {
            res.status(400).json({ error: 'User does not exists' });
        }

        if (oldPassword && !(await user.checkPassword(oldPassword))) {
            return res.status(401).json({ error: 'Password does not match' });
        }

        await user.update(
            { name, email, provider, password, avatar_id },
            { new: true }
        );

        const { id, avatar } = await User.findByPk(req.userId, {
            include: {
                model: File,
                as: 'avatar',
                attributes: ['id', 'path', 'url'],
            },
        });

        return res.json({
            id,
            name,
            email,
            avatar,
        });
    }
}

export default new UserController();
