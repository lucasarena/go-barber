import * as Yup from 'yup';
import { startOfHour, parseISO, isBefore, format, subHours } from 'date-fns';
import pt from 'date-fns/locale/pt';
import Appointment from '../models/Appointment';
import User from '../models/User';
import File from '../models/File';
import Notification from '../schemas/Notification';

import CancellationMail from '../jobs/CancellationMail';
import Queue from '../../lib/Queue';

class AppointmentController {
    async index(req, res) {
        const { page = 1 } = req.query;

        const appointments = await Appointment.findAll({
            where: {
                user_id: req.userId,
                canceled_at: null,
            },
            attributes: ['id', 'date', 'past', 'cancelable'],
            order: ['date'],
            limit: 20,
            offset: (page - 1) * 20,
            include: {
                model: User,
                as: 'provider',
                attributes: ['id', 'name'],
                include: {
                    model: File,
                    as: 'avatar',
                    attributes: ['id', 'path', 'url'],
                },
            },
        });

        res.json(appointments);
    }

    async store(req, res) {
        const schema = Yup.object().shape({
            date: Yup.date().required(),
            provider_id: Yup.number().required(),
        });

        if (!(await schema.isValid(req.body))) {
            res.status(400).json({ error: 'Validation failed' });
        }

        const { provider_id, date } = req.body;

        /**
         * Check if the user is a provider
         */
        const isProvider = await User.findOne({
            where: { id: provider_id, provider: true },
        });

        if (!isProvider) {
            res.status(401).json({
                error: 'Only provider can create appointments',
            });
        }

        const hourStart = startOfHour(parseISO(date));

        /**
         * Check for passed date
         */
        if (isBefore(hourStart, new Date())) {
            res.status(400).json({ error: 'Passed date is not permitted' });
        }

        /**
         * Check date availabilty
         */
        const checkAvailability = await Appointment.findOne({
            where: {
                provider_id,
                canceled_at: null,
                date: hourStart,
            },
        });

        if (checkAvailability) {
            return res
                .status(400)
                .json({ error: 'Appointment date is no available' });
        }

        if (req.userId === provider_id) {
            return res.status(400).json({
                error: 'The user cannot make a appoitment for itself',
            });
        }

        const appointment = await Appointment.create({
            user_id: req.userId,
            provider_id,
            date: hourStart,
        });

        /**
         * Notify provider
         */

        const user = await User.findByPk(req.userId);

        const formattedDate = format(
            hourStart,
            "'dia' dd 'de' MMMM 'ás' H:MM'h'",
            { locale: pt }
        );

        await Notification.create({
            content: `Novo agendamento de ${user.name} para ${formattedDate}`,
            user: provider_id,
        });

        return res.json(appointment);
    }

    async delete(req, res) {
        const appointment = await Appointment.findByPk(req.params.id, {
            include: [
                {
                    model: User,
                    as: 'provider',
                    attributes: ['name', 'email'],
                },
                {
                    model: User,
                    as: 'user',
                    attributes: ['name'],
                },
            ],
        });

        if (appointment.canceled_at) {
            res.status(400).json({ error: 'Appointment already canceled' });
        }

        if (appointment.user_id !== req.userId) {
            res.status(401).json({
                error: 'You can not canceled other user appointment',
            });
        }

        const dateWithSub = subHours(appointment.date, 2);

        if (isBefore(dateWithSub, new Date())) {
            res.status(401).json({
                error: 'You can only canceled appointments 2 hours early',
            });
        }

        await appointment.update({
            canceled_at: new Date(),
        });

        await Queue.add(CancellationMail.key, {
            appointment,
        });

        return res.json(appointment);
    }
}

export default new AppointmentController();
