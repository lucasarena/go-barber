import jwt from 'jsonwebtoken';

import { promisify } from 'util';

import authConfig from '../../config/auth';

export default async (req, res, next) => {
    const headerAuthorizathion = req.headers.authorization;

    if (!headerAuthorizathion) {
        return res.status(401).json({ error: 'Token not provided' });
    }

    const [, token] = headerAuthorizathion.split(' ');

    try {
        const tokenInfo = await promisify(jwt.verify)(token, authConfig.secret);
        req.userId = tokenInfo.id;
        return next();
    } catch (error) {
        res.status(401).json({ error: 'Invalid Token' });
    }
};
